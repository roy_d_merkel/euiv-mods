east_slavic = {
	ilmenian = {
		primary = NOV
		dynasty_names = { }
	}
	volhynian = {
		primary = KIE
		dynasty_names = { }
	}
	severian = {
		primary = KIE
		dynasty_names = { }
	}
	russian_culture = {
		primary = RUS
		dynasty_names = { "Dregovich" "Boryatinskiy" "Danilovich" "Feofilaktovich" "Vyshatich" "Borisov" "Rtishchev" "Kobylin" "Iziaslavich" "Rtishchev" "Zubvich" "Artamonovich" "Kobylin" "Yurievich" "Shuvalov" "Avanich" "Kromid" "Borisov" "Miroslavich" "Izyaslavich" "Potyomkin" "Pankeyev" "Pushkin" "Ovinov" "Rumyanets" "Klimovich" "Chernyshev" "Potyomkin" "Kievskiy" "Vyshatich" "Konstantinid" "Yaroslavich" "Shafirov" "Skuratov" "Rtishchev" "Paskevich" "Meshchersky" "Artamonovich" "Gdovich" "Shein" "Ivanovich" "Vyshatich" "Neplyuyev" "of Terebovl" "Izyaslavich" "Kromid" "Shafirov" "Hrodnich" "Meshchersky" "Ovinov" "Yaroslavich" "Sobakin" "Kholmsky" "Golovin" "Konstantinov" "Volhynsky" "Gdovich" "Ivankovich" "Matveev" "Yurievich" "Karachid" "Skuratov" "Kozelid" "Demidov" "Obolensky" "Dmitrievich" "Gdovich" "Karachid" "Kievskiy" "Pushkin" "Shemyaka" "Kholmsky" "Rtishchev" "Vseslavich" "Mikhailovich" "Vozviahich" "Lukinich" "Khitrov" "Poluektovich" "Kirillid" }
	}
	carantanian = {
		primary = SLO
		dynasty_names = { "Ivkovic" "Vukcic" "Milenic" "Carantania" }
	}
	bosnian = {
		primary = BOS
		dynasty_names = { "Vukovic" "Kristic" "Vojsalic" "Hvaeonic" "Radivojevic" "Radivojevic" }
	}
}